import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { User } from '@/_models';
import { AlertService, UserService, AuthenticationService } from '@/_services';
import { HttpClient  } from '@angular/common/http';
@Component({templateUrl: 'audit.component.html'})
export class AuditComponent implements OnInit {

    allUsers:User[] = [];
    p: number = 1;
    ipAddress;
    constructor(
        private userService: UserService,
        private http:HttpClient,
        private service :AuthenticationService 
    ){
       
    }
    ngOnInit() {
        this.userService.auditUser().pipe(first()).subscribe(users => {
            this.allUsers = users;
        
        });
    }

    
}